FROM debian-dev-base as dev-base 
LABEL author="Luke Roberts" 

# Install a couple of python libraries necessary for 
# the SkyNet reader package
RUN pip3 install --upgrade pip \
    && pip3 install --no-binary=h5py h5py \
    && pip3 install mendeleev