import numpy as np 
import h5py 
from mendeleev import element
import scipy as sp
import scipy.interpolate
from scipy.integrate import solve_ivp

def GetAbundancesFixedYef(Ytot, T9, rho, lnYef, xi, useNeutral = False):
    """
    For numpy arrays of total elemental abundances Ytot, temperatures T9 (in 
    GK), densities rho (in g/cc), and natural log of the free electron fractions
    lnYef for a set of times, calculate the abundances of the ionization states 
    defined by the ionization potentials given in the array xi 
    
    useNeutral determines which abundance to scale relative to, probably always 
    want to keep this set to False except for testing purposes.
    
    returns an array YI(len(Ytot), len(xi)+1) of the abundances 
    """
    
    # Define some constants
    Na = 6.02e23 # Avogadros number
    me = 9.109e-28 # Electron mass in grams 
    Kb = 1.3806e-16 # Boltzmann's constants in erg/K
    Kbev = Kb/1.602e-12 # Boltzmann's constant in ev/K
    hbar = 1.0545e-27 # Planck's constant in erg s

    # Calculate the log of the g function for all ionization states except for 
    # the fully ionized state 
    #lng = np.zeros((len(T9), len(xi))) 
    #for (i, x) in enumerate(xi):
    #    lng[:,i] = np.log(2/(rho*Na)*(me*Kb*1.e9*T9/(2*np.pi*hbar*hbar))**(3/2)) 
    #    lng[:,i] += - x/(Kbev*1.e9*T9) - lnYef
    lng = np.outer(np.log(2/(rho*Na)*(me*Kb*1.e9*T9/(2*np.pi*hbar*hbar))**(3/2))
        -lnYef, np.ones(len(xi)))
    lng = lng - np.outer(1.0/(Kbev*1.e9*T9), xi) 

    # Calculate the sum of ln g for every ionization state 
    lnh = np.zeros((len(T9), len(xi)+1)) 
    lnh[:,0] = 0.0
    for (i, x) in enumerate(xi):
        lnh[:,i+1] = lnh[:,i] + lng[:,i]

    # Find the maximum value and ionization state index of h for each time 
    idxMax = np.argmax(lnh, 1)
    lnhMax = np.amax(lnh, 1)

    # Work in terms of the unionized state 
    if useNeutral:
        idxMax = np.zeros(T9.shape) 
        lnhMax = np.zeros(T9.shape)
    
    # Find the scaled h factors for converting from maximum ionization state 
    #abundance to any other abundance 
    lnhScaled = lnh 
    for i in range(len(xi)+1):
        lnhScaled[:,i] = lnh[:,i] - lnhMax[:]

    # Calculate the abundance of the maximum ionization state 
    Ymax = Ytot/np.sum(np.exp(lnhScaled), 1)  

    # Calculate the abundance of all other ionization states based on maximum 
    #ionization state
    #YI = Ymax
    YI = np.zeros((len(T9), len(xi)+1)) 
    for i in range(len(xi)+1): 
        YI[:,i] = Ymax[:]*np.exp(lnhScaled[:,i])
    return YI 

def GetYef(Ytot, T9, rho, xi, niter=25, lnYeMin=-400.0): 
    """ 
    Calculates the abundances of various ionization states by self-consistently 
    finding the free electron fraction assuming charge neutrality 
    
    We use bisection since it is simple to use on the entire numpy array at the 
    same time.
    """
    
    # Set the initial range of allowed electron fractions
    lnYefLow = lnYeMin*np.ones(T9.shape) 
    lnYefHi = np.zeros(T9.shape) 
    def GetYefContribution(Ytot, T9, rho, lnYeg, xi):
        """
        For a given guess for the free electron fraction, calculate the free 
        electron fraction contributed by an element with total abundance Ytot 
        and ionization energies xi assuming charge neutrality
        """
        YefContribution = 0.0
        for (i, x) in enumerate(xi):
          YI = GetAbundancesFixedYef(Ytot[:,i], T9, rho, lnYeg, x)
          YefContribution += np.dot(YI, np.arange(0, len(x)+1, 1))
        return YefContribution
    
    # Build function to compare Yef determined by charge neutrality and by the imposed Yef 
    # Find real solution by finding roots of this equation
    fLow = np.log(GetYefContribution(Ytot, T9, rho, lnYefLow, xi)) - lnYefLow 
    fHi = np.log(GetYefContribution(Ytot, T9, rho, lnYefHi, xi)) - lnYefHi
     
    # We need to flag places where our function doesnt have opposite signs on either end of the interval 
    bad = np.where(fLow*fHi>0, 1.0, 0.0)
    
    # Perform niter bisection iterations
    for i in range(niter):
        # Take the midpoint of the lnYef range and calculate the charge neutrality electron fraction 
        # and calculate value of function we want to find the root of 
        lnYefMid = 0.5*(lnYefLow + lnYefHi) 
        fMid = np.log(GetYefContribution(Ytot, T9, rho, lnYefMid, xi)) - lnYefMid
        
        # Replace either the low or high end of the range with the midpoint value depending on 
        # the signs of the root function 
        lnYefLow = np.where(fMid*fLow>0, lnYefMid, lnYefLow)
        fLow = np.where(fMid*fLow>0, fMid, fLow)
        
        lnYefHi = np.where(fMid*fHi>0, lnYefMid, lnYefHi)
        fHi = np.where(fMid*fHi>0, fMid, fHi)
        
    lnYefMid = 0.5*(lnYefLow + lnYefHi)
    
    # Choose the minimum value of Ye when the root doesn't sit in the initial range
    # This is safe since the upper bound is truly an upper bound 
    lnYefMid = np.where(bad>0.5, lnYeMin, lnYefMid)
    
    # Return the free electron fraction
    return (np.exp(lnYefMid), GetYefContribution(Ytot, T9, rho, lnYefMid, xi))

"""
Define some stuff that is useful for integrating the temperature
"""
def log_interp1d(xx, yin, deriv = False, kind='linear'):
    """
    Returns a function object that returns values based on logarithmic interpolation
    If deriv is set true, an approximation to its first derivative is returned 
    """
    
    # Build the centered derivatives if asking for derivative of 
    # the function 
    if deriv: 
        yy = np.zeros(yin.shape)
        yy[1:-1] = (yin[2:] - yin[0:-2])/(xx[2:] - xx[0:-2])
        yy[0] = (yin[1] - yin[0])/(xx[1] - xx[0]) # Can't build centered derivatives at the bounds 
        yy[-1] = (yin[-1] - yin[-2])/(xx[-1] - xx[-2]) # Can't build the centered derivatives at the bounds
    else:
        yy = yin
    
    mult = 1.0
    # Check that all yy values have the same sign 
    if (yy > 0).all():
        mult = 1.0
    elif (yy < 0).all(): 
        mult = -1.0

    logx = np.log(xx)
    logy = np.log(mult*yy)
    lin_interp = sp.interpolate.interp1d(logx, logy, kind=kind)
    log_interp = lambda zz: mult*np.exp(lin_interp(np.log(zz)))
    return log_interp

def epsilon_gamma(rho, T9, Ytilde): # rho expected to be in [g/cc] 
    acgs = 7.5646e-15 # in [erg cm^-3 K^-4] 
    return acgs*np.power(T9*1.e9, 4)/rho # in [erg/g]

def epsilon_gas(rho, T9, Ytilde): # rho expected to be in [g/cc]
    kbcgs = 1.380658e-16 # in [erg K^-1]
    mbcgs = 1.6726231e-24 # in [g]
    return 3*Ytilde*kbcgs*1.e9*T9/(2*mbcgs) # in [erg/g]

class SkyNetIonization: 
    def __init__(self, fname, calculateYeFree = True): 
        f = h5py.File(fname,'r')
        
        Z = np.array(f['Z'])
        self.time = np.array(f['Time'])
        self.rho = np.array(f['Density'])
        self.qdot = np.array(f['HeatingRate'])

        # Read in the temperature and replace with a power law expansion
        T9Orig = np.array(f['Temperature'])
        idx = np.searchsorted(-T9Orig,-1.e-3)
        self.T9 = T9Orig
        self.T9[idx:] = self.T9[idx]*(self.time[idx]/self.time[idx:])
        
        # Calculate elemental abundances Ytot 
        nElements = np.max(Z)+1
        nTimes = self.T9.shape[0]
        Y = np.array(f['Y'])
        self.Ytot = np.zeros((nTimes, nElements))
        for (i, zz) in enumerate(Z):
            self.Ytot[:, zz] += Y[:, i]  
        
        # Calculate total Ye from SkyNet abundances 
        self.YeTot = np.zeros(self.T9.shape) 
        for zz in range(nElements):
            self.YeTot += zz*self.Ytot[:,zz]
        
        # Tabulate ionization potentials using the Mendeleev library 
        self.xi = [] 
        self.xi.append([]) # Account for neutrons
        for zz in range(1,nElements):
            self.xi.append(np.array(list(element(zz).ionenergies.values()))) # Use ionization energies from Mendeleev package
        
        
        self.T9 = self.IntegrateTemperature(doSaha=True, radiationOnly=False) 

        # Calculate the ionization state abundances with self-consistent temperature 
        if (calculateYeFree):
            self.ConsistentYeFreeSet = True 
            self.Yef = self.CalculateSelfConsistentYef()
        else: 
            self.ConsistentYeFreeSet = False
            self.Yef = self.YeTot
        
            
    def ExtrapolateTemperature(self, TExtrapFrom = 1.e-3, powerLaw = -1):
        T9Extrap = self.T9 
        idx = np.searchsorted(-self.T9, -TExtrapFrom)
        
        T9Extrap[idx:] = self.T9[idx]*np.power(self.time[idx:]/self.time[idx], powerLaw) 
        return T9Extrap
    
    def CalculateSelfConsistentYef(self):
        (Yef, YefSum) = GetYef(self.Ytot, self.T9, self.rho, self.xi) 
        return Yef

    def GetIonizationStateAbundancesElement(self, zz):
        return GetAbundancesFixedYef(self.Ytot[:,zz], self.T9, self.rho, 
                                     np.log(self.Yef), self.xi[zz])
   
    def GetIsoElectronicAbundances(self, zmin, zmax): 
        YI = np.zeros((self.T9.shape[0],zmax+1))
        for zz in range(zmin, zmax+1):
            YI[:,zmax-zz:] += self.GetIonizationStateAbundancesElement(zz)
        return YI

    def IntegrateTemperature(self, radiationOnly = False, doSaha = True):
        """
        Integrate the temperature using teh first law of thermodynamics 
        """
        # Calculate the total number of free nuclei per baryon 
        Ytilde = np.sum(self.Ytot, axis=1) 

        # Create some callable functions that interpolate various SkyNet quantities as a function of time
        T9old_func = log_interp1d(self.time, self.T9)
        rho_func = log_interp1d(self.time, self.rho)
        yeTot_func = log_interp1d(self.time, self.YeTot)
        rhoDot_func = log_interp1d(self.time, self.rho, deriv=True)
        yTilde_func = log_interp1d(self.time, Ytilde)
        qDot_func = log_interp1d(self.time, self.qdot)
        

        # Right hand side of the dlog(T9)/dt equation 
        def TEvolutionRHS(time, lT9): 
            T9 = np.exp(lT9) 
            rho = rho_func(time)
            rhoDot = rhoDot_func(time)
            yTilde = yTilde_func(time)
            delta = 1.e-6
            yTildeDot = (yTilde_func(time*(1.0+delta)) - yTilde_func(time*(1.0-delta)))/(2*delta*time) 
            qDot = qDot_func(time) 

            egam = epsilon_gamma(rho, T9, yTilde) 
            egas = np.zeros(egam.shape)

            dYefdT = 0.0
            Yef = 0.0
            if not radiationOnly and doSaha: 
                # Get the closest set of abundances to the current 
                # time rather than interpolating
                idx = (np.abs(self.time - time)).argmin() 
            
                # Calculate how many free electrons there are using the Saha equation
                # and the derivative wrt temperature 
                Yef, blah = GetYef(np.repeat(self.Ytot[idx:idx+1,:], 3, axis=0), 
                    np.array([T9*(1-delta), T9, T9*(1+delta)]), np.array([rho, rho, rho]), self.xi) 
                yTilde = yTilde + Yef[1] 
                dYefdT = (Yef[2] - Yef[0])/(2*delta*T9)                    

                # Add on the rest of the time derivative of tilde Y holding T constant
                if idx+1 < len(self.time) and idx>0:
                    tm = self.time[idx-1]
                    tp = self.time[idx+1]
                    Yef, blah = GetYef(self.Ytot[idx-1:idx+2:2,:], np.array([T9, T9]),  
                        np.array([rho_func(tm), rho_func(self.time[idx+1])]), self.xi) 
                    yTildeDot += (Yef[1] - Yef[0])/(tp - tm)

                egas = epsilon_gas(rho, T9, yTilde)
            elif not radiationOnly: 
                # Don't do saha, just assume all electrons are unbound
                yTilde += yeTot_func(time)
                yTildeDot += (yeTot_func(time*(1.0+delta)) - yeTot_func(time*(1.0+delta)))/(2*delta*time)  
                dYefdT = 0.0
                Yef = yeTot_func(time)
                egas = epsilon_gas(rho, T9, yTilde)


            r = egas/egam 
            print ('{:e} {:e} {:e} {:e} {:e}'.format(time, Yef, yTilde, T9, epsilon_gas(rho, T9, yTilde + yeTot_func(time))/egam))
            rhs = (4 + 2*r)/(4 + r)*rhoDot/(3*rho) - r/(4 + r)*yTildeDot/yTilde + 1/(4 + r)*qDot/egam
            rhs = rhs/(1 + r/(4+r)*T9/yTilde*dYefdT) 
            
            return rhs
        
        # Integrate the temperature evolution equation 
        t0 = 1 
        T90 = T9old_func(t0) 
        tmax = max(self.time)*0.9999
        sol = solve_ivp(lambda t, xx: TEvolutionRHS(t, xx[0]), [t0, tmax], 
                [np.log(T90)], vectorized=False)
        T9out_func = log_interp1d(sol.t, np.exp(sol.y[0,:])) 

        # Replace the original temperature with integrated temperatures   
        T9out = np.zeros(self.time.shape)    
        for (i, tt) in enumerate(self.time):                 
            if (tt>t0): T9out[i] = T9out_func(tt*0.9998) 
            else: T9out[i] = self.T9[i]
        return T9out